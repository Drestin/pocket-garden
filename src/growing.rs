use bevy::prelude::*;

use crate::plants::Plant;

///////

struct GrowTickTimer(Timer);

fn update_plant_age(time: Res<Time>, mut q: Query<&mut Plant>) {
    for mut plant in q.iter_mut() {
        plant.age += time.delta();
    }
}

///////

pub struct GrowingPlugin;

impl Plugin for GrowingPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(GrowTickTimer(Timer::from_seconds(1.0, true)))
            .add_system(update_plant_age);
    }
}
