use std::f32::consts::TAU;

use bevy::{prelude::*, utils::HashMap};

use crate::{
    input::{MouseTargetKind, MouseTargetRoot},
    util::urand_range,
};

#[derive(Component, Debug, PartialEq, Eq, Hash, Clone)]
pub struct GridPos {
    pub x: i32,
    pub y: i32,
}

impl Default for GridPos {
    fn default() -> Self {
        Self { x: 0, y: 0 }
    }
}

impl GridPos {
    pub fn neighbours(&self) -> Vec<Self> {
        vec![
            Self {
                x: self.x - 1,
                y: self.y,
            },
            Self {
                x: self.x,
                y: self.y - 1,
            },
            Self {
                x: self.x + 1,
                y: self.y,
            },
            Self {
                x: self.x,
                y: self.y + 1,
            },
        ]
    }
}

#[derive(Component)]
pub struct Tile;

#[derive(Default)]
pub struct Grid {
    tiles: HashMap<GridPos, Entity>,
    plants: HashMap<GridPos, Entity>,
}

impl Grid {
    pub fn set_tile(&mut self, grid_pos: &GridPos, tile_entity: Entity) -> Option<Entity> {
        self.tiles.insert(grid_pos.clone(), tile_entity)
    }

    pub fn get_tile(&self, grid_pos: &GridPos) -> Option<Entity> {
        self.tiles.get(grid_pos).map(|e| e.clone())
    }

    pub fn set_plant(&mut self, grid_pos: &GridPos, plant_entity: Entity) -> Option<Entity> {
        self.plants.insert(grid_pos.clone(), plant_entity)
    }

    pub fn _delete_plant(&mut self, grid_pos: &GridPos) -> Option<Entity> {
        self.plants.remove(grid_pos)
    }

    pub fn get_plant(&self, grid_pos: &GridPos) -> Option<Entity> {
        self.plants.get(grid_pos).map(|e| e.clone())
    }
}

fn create_garden(mut commands: Commands, asset_server: Res<AssetServer>, mut grid: ResMut<Grid>) {
    for x in 0..=0 {
        for y in 0..=0 {
            create_tile(&GridPos { x, y }, &mut commands, &asset_server, &mut grid);
        }
    }
}

pub fn create_tile(
    grid_pos: &GridPos,
    commands: &mut Commands,
    asset_server: &AssetServer,
    grid: &mut Grid,
) {
    if grid.get_tile(grid_pos).is_some() {
        eprintln!("Trying to create a tile at occupied pos {:?}", grid_pos);
        return;
    }

    let cell_scene = asset_server.load("cell.glb#Scene0");
    let tile_entity = commands
        .spawn_bundle(TransformBundle::from(Transform::from_rotation(
            Quat::from_rotation_y(0.25 * TAU * urand_range(0, 3) as f32),
        )))
        .insert(grid_pos.clone())
        .insert(Tile)
        .insert(MouseTargetRoot {
            kind: MouseTargetKind::TILE,
        })
        .with_children(|p| {
            p.spawn_scene(cell_scene.clone());
        })
        .id();
    grid.set_tile(grid_pos, tile_entity);
}

fn position_system(mut query: Query<(&GridPos, &mut Transform), Changed<GridPos>>) {
    for (grid_pos, mut transform) in query.iter_mut() {
        transform.translation = Vec3::new(grid_pos.x as f32, 0.0, grid_pos.y as f32)
    }
}

pub struct GridPlugin;

impl Plugin for GridPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Grid>()
            .add_startup_system(create_garden)
            .add_system(position_system);
    }
}
