mod grid;
mod growing;
mod gui;
mod input;
mod plants;
mod scene;
mod util;

use bevy::prelude::*;
use bevy_egui::EguiPlugin;
use growing::GrowingPlugin;
use plants::PlantsPlugin;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(EguiPlugin)
        .add_plugin(PlantsPlugin)
        .add_plugin(gui::UiPlugin)
        .add_plugin(scene::SceneSetupPlugin)
        .add_plugin(grid::GridPlugin)
        .add_plugin(input::InputPlugin)
        .add_plugin(GrowingPlugin)
        .run();
}
