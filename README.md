# Pocket garden

Jardin collaboratif, sandbox.

## Intentions

Apprendre le Rust, les ECS, et créer un jeu sans éditeur visuel (style Unity)

## Fonctionalités
- Sandbox
- 3D isométrique
- Jardin :
  - planter des plantes
  - regarder grandir, complentatif
  - entretenir ?
- Pas de productivité, chill, comtemplatif
- En ligne :
  - regarder le jardin des autres
  - modifier aussi (option jardin protégé)
- Cycle jour-nuit
- Gameplay autour de combo de plantation => pour faire apparaître des plantes rares.

## Idées pour plus tard
- jardin infini qui s'étend avec les plantes qu'on plante
- iframe qui affiche un jardin en fonction du nom de domaine (si on a accès ?)
- Mettre des commentaires sur le jardin des autres (modération ?)
- Météo
- Un seul grand jardin ?

## Roadmap
### Première itération
[x] Créer projet Rust + Bevy
[x] Afficher un carré de jardin en 3D isométrique
[ ] Pouvoir cliquer pour planter une plante
[ ] La plante pousse (quelques états, des ronds)


### Plus joli
[ ] Assets de plante
[ ] Animation à la poussée
[ ] Animation idle
[ ] Rend plus joli le reste (fond, sol)

...

### En ligne, aspect collaboratif
[ ] Connecter à la base CouchDB (passer par le Js pour gérer le réseau ?)
[ ] Sauvegarder le jardin sur CouchDB
[ ] Synchroniser la vue à l'ouverture de la page
[ ] Synchroniser les changements avec un événement de CouchDB
[ ] Envoyer des événements à la base à chaque modification
[ ] Comment gérer la propriété de son jardin ? (anarchie, login, clé sur navigateur, mot de passe pour modifier) Qui est le master ?
