use std::f32::consts::TAU;

use bevy::{
    input::mouse::{MouseMotion, MouseWheel},
    prelude::*,
    render::camera::Camera3d,
};
use bevy_mod_raycast::RayCastSource;

use crate::{input::MouseRayCastTarget, util::Vec2Extras};

#[derive(Component)]
struct CameraAndScene;

fn setup_3d(mut commands: Commands, asset_server: Res<AssetServer>) {
    // light
    commands.spawn_bundle(DirectionalLightBundle {
        directional_light: DirectionalLight {
            illuminance: 50_000.0,
            shadows_enabled: true,
            shadow_projection: OrthographicProjection {
                near: -100.0,
                far: 100.0,
                scale: 4.0,
                ..default()
            },
            ..default()
        },
        transform: Transform::from_xyz(-5.0, 10.0, 2.5).looking_at(Vec3::ZERO, Vec3::Y),
        ..default()
    });

    commands.insert_resource(AmbientLight {
        color: Color::WHITE,
        brightness: 0.55,
    });

    commands
        .spawn_bundle(TransformBundle::default())
        .insert(CameraAndScene)
        .with_children(|p| {
            // camera
            let mut camera = OrthographicCameraBundle {
                transform: Transform::from_xyz(-5.0, 5.0, -5.0)
                    .looking_at(Vec3::new(0.0, 0.5, 0.0), Vec3::Y),
                ..OrthographicCameraBundle::new_3d()
            };
            camera.orthographic_projection.scale = 3.0;
            p.spawn_bundle(camera)
                .insert(RayCastSource::<MouseRayCastTarget>::new());

            // Scene
            p.spawn_scene(asset_server.load("base.glb#Scene0"));
        });
}

fn scroll_zoom_system(
    mut scroll_evr: EventReader<MouseWheel>,
    mut query_camera: Query<&mut OrthographicProjection, With<Camera3d>>,
) {
    use bevy::input::mouse::MouseScrollUnit;
    if !scroll_evr.is_empty() {
        if let Ok(mut projection) = query_camera.get_single_mut() {
            for ev in scroll_evr.iter() {
                let coef = match ev.unit {
                    MouseScrollUnit::Line => 10.0,
                    MouseScrollUnit::Pixel => 1.0,
                };
                let delta = -0.04 * coef * ev.y;
                projection.scale = f32::clamp(projection.scale + delta, 0.1, 5.8);
            }
        }
    }
}

fn camera_pan_system(
    mouse_buttons: Res<Input<MouseButton>>,
    mut mouse_move_evr: EventReader<MouseMotion>,
    query_camera: Query<&OrthographicProjection, With<Camera3d>>,
    mut query_scene_transform: Query<&mut Transform, With<CameraAndScene>>,
    mut windows: ResMut<Windows>,
) {
    let window = windows.get_primary_mut().unwrap();

    if mouse_buttons.pressed(MouseButton::Middle) {
        window.set_cursor_lock_mode(true);
        window.set_cursor_visibility(false);

        if !mouse_move_evr.is_empty() {
            if let Ok(projection) = query_camera.get_single() {
                if let Ok(mut scene_transform) = query_scene_transform.get_single_mut() {
                    for ev in mouse_move_evr.iter() {
                        let mut modified_delta = ev.delta.clone();
                        modified_delta.y *= 2.0;
                        let rotated_delta = modified_delta.rotated(-0.125 * TAU);
                        scene_transform.translation += 0.003
                            * projection.scale
                            * Vec3::new(rotated_delta.x, 0.0, rotated_delta.y);
                    }
                }
            }
        }
    } else {
        window.set_cursor_lock_mode(false);
        window.set_cursor_visibility(true);
    }
}

pub struct SceneSetupPlugin;

impl Plugin for SceneSetupPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.insert_resource(Msaa { samples: 4 })
            .add_startup_system(setup_3d)
            .add_system(scroll_zoom_system)
            .add_system(camera_pan_system);
    }
}
