use std::time::Duration;

use bevy::{
    pbr::NotShadowCaster,
    prelude::{shape, *},
};

use crate::{
    grid::{Grid, GridPos},
    gui::UiState,
    plants::{CreatePlantEvent, SpeciesServer},
};

use super::{raw_input::TargettedTileChangeEvent, TargettedTile};

pub struct UserInputPlugin;

impl Plugin for UserInputPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(create_hint_system)
            .add_system(update_hint_system.after("mouse"))
            .add_system(click_to_plant_system.after(update_hint_system));
    }
}

fn click_to_plant_system(
    mouse_button_input: Res<Input<MouseButton>>,
    grid: Res<Grid>,
    targetted_tile: Res<TargettedTile>,
    mut plant_event_writer: EventWriter<CreatePlantEvent>,
    ui_state: Res<UiState>,
    species_server: Res<SpeciesServer>,
) {
    if mouse_button_input.just_pressed(MouseButton::Left) {
        if let Some(pos) = &targetted_tile.position {
            println!("Clicked at pos {:?}", &pos);
            if grid.get_plant(&pos).is_none() {
                plant_event_writer.send(CreatePlantEvent {
                    position: pos.clone(),
                    initial_age: Duration::ZERO,
                    species: species_server.get(&ui_state.selected_plant_species).clone(),
                })
            }
        }
    }
}

#[derive(Component)]
struct MouseHint;

fn create_hint_system(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands
        .spawn_bundle(TransformBundle::default())
        .insert(GridPos::default())
        .insert(MouseHint)
        .insert(NotShadowCaster)
        .with_children(|p| {
            p.spawn_bundle(PbrBundle {
                mesh: meshes.add(Mesh::from(shape::Box::new(1.01, 0.51, 1.01))),
                visibility: Visibility { is_visible: false },
                material: materials.add(StandardMaterial {
                    base_color: Color::rgba(1.0, 1.0, 1.0, 0.3),
                    cull_mode: None,
                    alpha_mode: AlphaMode::Blend,
                    ..default()
                }),
                transform: Transform::from_xyz(0.0, -0.25, 0.0),
                ..default()
            });
        });
}

fn update_hint_system(
    mut event_reader: EventReader<TargettedTileChangeEvent>,
    mut query_hint: Query<(&mut GridPos, &Children), With<MouseHint>>,
    mut query_visibility: Query<&mut Visibility>,
) {
    for event in event_reader.iter() {
        for (mut hint_pos, children) in query_hint.iter_mut() {
            for child in children.iter() {
                for mut visibility in query_visibility.get_mut(*child) {
                    match &event.0 {
                        None => visibility.is_visible = false,
                        Some(pos) => {
                            hint_pos.clone_from(pos);
                            visibility.is_visible = true;
                        }
                    }
                }
            }
        }
    }
}
