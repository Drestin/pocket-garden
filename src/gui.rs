use bevy::prelude::*;
use bevy_egui::*;

use crate::plants::{SpeciesName, SpeciesServer};

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<UiState>().add_system(ui_example);
    }
}

fn ui_example(
    mut egui_context: ResMut<EguiContext>,
    mut ui_state: ResMut<UiState>,
    species_server: Res<SpeciesServer>,
) {
    egui::Window::new("Plantes").show(egui_context.ctx_mut(), |ui| {
        for species in species_server.into_iter() {
            ui.radio_value(
                &mut ui_state.selected_plant_species,
                species.name.clone(),
                species.name.0.clone(),
            );
        }
    });
}

pub struct UiState {
    pub selected_plant_species: SpeciesName,
}

impl FromWorld for UiState {
    fn from_world(world: &mut World) -> Self {
        let species_server = world.get_resource::<SpeciesServer>().unwrap();
        Self {
            selected_plant_species: species_server.default_species().name.clone(),
        }
    }
}
