use bevy::prelude::Plugin;

use self::{raw_input::RawInputPlugin, user_input::UserInputPlugin};

mod raw_input;
mod user_input;

pub use raw_input::{MouseRayCastTarget, MouseTargetKind, MouseTargetRoot, TargettedTile};

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugin(RawInputPlugin).add_plugin(UserInputPlugin);
    }
}
