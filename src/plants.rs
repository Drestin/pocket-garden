use std::{f32::consts::TAU, time::Duration};

use bevy::prelude::*;

use crate::{
    grid::{create_tile, Grid, GridPos},
    util::frand_range,
};

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum PlantMaturity {
    PLANTED,
    GROWING,
    MATURE,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct SpeciesName(pub String);

impl From<&str> for SpeciesName {
    fn from(s: &str) -> Self {
        SpeciesName(String::from(s))
    }
}

#[derive(Clone)]
pub struct Species {
    pub name: SpeciesName,
    file: String,
    growing_age: Duration,
    mature_age: Duration,
}

pub struct SpeciesServer {
    data: Vec<Species>,
}

impl SpeciesServer {
    fn new() -> Self {
        SpeciesServer { data: Vec::new() }
    }

    fn register_species(&mut self, species: Species) {
        self.data.push(species);
    }

    pub fn default_species(&self) -> &Species {
        self.data.get(0).unwrap()
    }

    pub fn get(&self, name: &SpeciesName) -> &Species {
        self.data.iter().find(|e| e.name == *name).unwrap()
    }
}

impl FromWorld for SpeciesServer {
    fn from_world(_world: &mut World) -> Self {
        let mut species_server = Self::new();

        species_server.register_species(Species {
            name: SpeciesName::from("Tulipe"),
            file: String::from("tulipe.glb"),
            growing_age: Duration::from_secs(5),
            mature_age: Duration::from_secs(10),
        });

        species_server.register_species(Species {
            name: SpeciesName::from("Rose"),
            file: String::from("rose.glb"),
            growing_age: Duration::from_secs(8),
            mature_age: Duration::from_secs(15),
        });

        species_server
    }
}

impl<'a> IntoIterator for &'a SpeciesServer {
    type Item = &'a Species;
    type IntoIter = <&'a Vec<Species> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        (&self.data).into_iter()
    }
}

#[derive(Component)]
pub struct Plant {
    pub age: Duration,
    pub species: Species,
}

impl Plant {
    fn get_maturity(&self) -> PlantMaturity {
        match self.age {
            a if a > self.species.mature_age => PlantMaturity::MATURE,
            a if a > self.species.growing_age => PlantMaturity::GROWING,
            _ => PlantMaturity::PLANTED,
        }
    }
}

#[derive(Component, Clone)]
struct PlantMaturityMesh(Option<PlantMaturity>);

impl Default for PlantMaturityMesh {
    fn default() -> Self {
        Self(None)
    }
}

#[derive(Bundle)]
struct PlantBundle {
    pub plant: Plant,
    pub grid_position: GridPos,
    #[bundle]
    transform_bundle: TransformBundle,
}

//////

pub struct CreatePlantEvent {
    pub position: GridPos,
    pub initial_age: Duration,
    pub species: Species,
}

//////

fn create_plants(
    mut er: EventReader<CreatePlantEvent>,
    mut commands: Commands,
    mut grid: ResMut<Grid>,
    asset_server: Res<AssetServer>,
) {
    for e in er.iter() {
        if grid.get_plant(&e.position).is_some() {
            println!(
                "Wanted to create plant at occupied space {:?}",
                e.position.clone()
            );
            continue;
        }

        let plant_entity = commands
            .spawn_bundle(PlantBundle {
                plant: Plant {
                    age: e.initial_age,
                    species: e.species.clone(),
                },
                grid_position: e.position.clone(),
                transform_bundle: TransformBundle::default(),
            })
            .with_children(|p| {
                p.spawn_bundle(TransformBundle::from(Transform {
                    rotation: Quat::from_euler(
                        EulerRot::YZX,
                        frand_range(0.0, TAU),
                        frand_range(-0.05, 0.05),
                        frand_range(-0.05, 0.05),
                    ),
                    scale: frand_range(0.9, 1.1) * Vec3::ONE,
                    translation: Vec3::new(frand_range(-0.2, 0.2), 0.0, frand_range(-0.2, 0.2)),
                }))
                .insert(PlantMaturityMesh::default());
            })
            .id();
        grid.set_plant(&e.position, plant_entity);
        println!(
            "Created {:?} at {:?} aged of {:?}",
            e.species.name,
            e.position.clone(),
            e.initial_age.as_secs()
        );

        for pos in e.position.neighbours() {
            if grid.get_tile(&pos).is_none() {
                create_tile(&pos, &mut commands, &asset_server, &mut grid)
            }
        }
    }
}

fn update_plants_meshes(
    mut q: Query<(&mut PlantMaturityMesh, &Parent, Entity)>,
    parentq: Query<&Plant, Changed<Plant>>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    for (mut maturity, parent, entity) in q.iter_mut() {
        match parentq.get(parent.0) {
            Ok(plant) => {
                let new_maturity = plant.get_maturity();

                let need_respawn = match maturity.0 {
                    Some(mesh_maturity) => mesh_maturity != new_maturity,
                    None => true,
                };

                if need_respawn {
                    commands.entity(entity).despawn_descendants();

                    maturity.0 = Some(new_maturity);
                    let scene_index = match new_maturity {
                        PlantMaturity::PLANTED => 0,
                        PlantMaturity::GROWING => 1,
                        PlantMaturity::MATURE => 2,
                    };

                    commands.entity(entity).with_children(|p| {
                        p.spawn_scene(
                            asset_server
                                .load(&format!("{}#Scene{}", plant.species.file, scene_index)),
                        );
                    });
                }
            }
            Err(_) => {}
        }
    }
}

//////

pub struct PlantsPlugin;

impl Plugin for PlantsPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<SpeciesServer>()
            .add_event::<CreatePlantEvent>()
            .add_system(create_plants)
            .add_system(update_plants_meshes);
    }
}
