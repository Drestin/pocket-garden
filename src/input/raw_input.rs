use bevy::{
    ecs::query::{Fetch, FilterFetch, WorldQuery},
    prelude::*,
};
use bevy_mod_raycast::*;

use crate::grid::GridPos;

pub struct RawInputPlugin;

impl Plugin for RawInputPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<TargettedTileChangeEvent>()
            .init_resource::<TargettedTile>()
            .add_plugin(DefaultRaycastingPlugin::<MouseRayCastTarget>::default())
            // .add_startup_system(setup_mouse_debug)
            .add_system_to_stage(CoreStage::First, update_raycast_with_cursor)
            .add_system_set(
                SystemSet::new()
                    .label("mouse")
                    .with_system(mark_mouse_pickable_meshes_system)
                    .with_system(mouse_point_system)
                    .with_system(targetted_tile_changed_system.after(mouse_point_system)),
            );
    }
}

fn _setup_mouse_debug(mut commands: Commands) {
    commands
        .insert_resource(DefaultPluginState::<MouseRayCastTarget>::default().with_debug_cursor());
}

#[derive(Default)]
pub struct TargettedTile {
    pub position: Option<GridPos>,
    changed: bool,
}

fn mouse_point_system(
    query_source: Query<&RayCastSource<MouseRayCastTarget>>,
    query_child_info: Query<&MouseTargetChildInfo>,
    query_root_infos: Query<(&GridPos, &MouseTargetRoot)>,
    mut targetted_tile: ResMut<TargettedTile>,
) {
    let new_pos = match query_source.single().intersect_top() {
        None => None,
        Some((mesh_entity, _)) => match query_child_info.get(mesh_entity) {
            Err(_) => {
                eprintln!("Mouse target but no MouseTargetChildInfo");
                None
            }
            Ok(info) => match query_root_infos.get(info.root) {
                Err(_) => {
                    eprintln!("clicked but couldn't get pos info");
                    None
                }
                Ok((pos, root)) => {
                    if root.kind == MouseTargetKind::TILE {
                        Some(pos.clone())
                    } else {
                        None
                    }
                }
            },
        },
    };

    if new_pos != targetted_tile.position {
        targetted_tile.position = new_pos;
        targetted_tile.changed = true;
    }
}

pub struct MouseRayCastTarget;

#[derive(Component, Clone, Copy, PartialEq, Eq)]
pub enum MouseTargetKind {
    TILE,
}

#[derive(Component)]
struct MouseTargetChildInfo {
    root: Entity,
}

#[derive(Bundle)]
pub struct MouseTargetBundle {
    marker: RayCastMesh<MouseRayCastTarget>,
    child_info: MouseTargetChildInfo,
}

impl MouseTargetBundle {
    pub fn new(root: Entity) -> Self {
        MouseTargetBundle {
            child_info: MouseTargetChildInfo { root },
            marker: RayCastMesh::<MouseRayCastTarget>::default(),
        }
    }
}

// Update our `RayCastSource` with the current cursor position every frame.
fn update_raycast_with_cursor(
    mut cursor: EventReader<CursorMoved>,
    mut query: Query<&mut RayCastSource<MouseRayCastTarget>>,
) {
    // Grab the most recent cursor event if it exists:
    let cursor_position = match cursor.iter().last() {
        Some(cursor_moved) => cursor_moved.position,
        None => return,
    };

    for mut raycast_source in &mut query.iter_mut() {
        raycast_source.cast_method = RayCastMethod::Screenspace(cursor_position);
    }
}

#[derive(Component)]
pub struct MouseTargetRoot {
    pub kind: MouseTargetKind,
}

fn mark_mouse_pickable_meshes_system(
    mut commands: Commands,
    query_parent: Query<&Parent>,
    query_added_mesh: Query<Entity, Added<Handle<Mesh>>>,
    query_root: Query<Entity, With<MouseTargetRoot>>,
) {
    for entity in query_added_mesh.iter() {
        if let Some(root) = search_in_ancestors(entity, &query_parent, &query_root) {
            commands
                .entity(entity)
                .insert_bundle(MouseTargetBundle::new(root));
        }
    }
}

fn search_in_ancestors<'w, 's, Q, F>(
    entity: Entity,
    query_parent: &Query<&Parent>,
    query_component: &'w Query<'_, 's, Q, F>,
) -> Option<<Q::ReadOnlyFetch as Fetch<'w, 's>>::Item>
where
    Q: WorldQuery,
    F: WorldQuery,
    F::Fetch: FilterFetch,
{
    match query_component.get(entity) {
        Ok(r) => Some(r),
        Err(_) => match query_parent.get(entity) {
            Ok(p) => search_in_ancestors(p.0, query_parent, query_component),
            Err(_) => None,
        },
    }
}

pub struct TargettedTileChangeEvent(pub Option<GridPos>);

fn targetted_tile_changed_system(
    mut targetted_tile: ResMut<TargettedTile>,
    mut event_writer: EventWriter<TargettedTileChangeEvent>,
) {
    if targetted_tile.changed {
        event_writer.send(TargettedTileChangeEvent(targetted_tile.position.clone()));
        targetted_tile.changed = false;
    }
}
