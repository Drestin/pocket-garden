use bevy::math::Vec2;
use rand::{self, prelude::IteratorRandom, thread_rng};

pub fn frand_range(from: f32, to: f32) -> f32 {
    rand::random::<f32>() * (to - from) + from
}

pub fn _irand_range(from: i32, to: i32) -> i32 {
    (from..=to).choose(&mut thread_rng()).unwrap()
}

pub fn urand_range(from: u32, to: u32) -> u32 {
    (from..=to).choose(&mut thread_rng()).unwrap()
}

pub trait Vec2Extras {
    fn rotated(&self, angle: f32) -> Self;
}

impl Vec2Extras for Vec2 {
    fn rotated(&self, angle: f32) -> Self {
        Self::new(
            self.x * f32::cos(angle) - self.y * f32::sin(angle),
            self.x * f32::sin(angle) + self.y * f32::cos(angle),
        )
    }
}
